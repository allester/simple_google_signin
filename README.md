# Simple Google Sign-in

## Technologies

-   Node.js

## Setup

Run `git clone https://gitlab.com/allester/simple_google_signin.git` to clone the repository to your local environment

`cd` into the project folder and run `npm install` to install the project dependencies

Create a `.env` file and add the following:

-   PORT=
-   SESSION_SECRET=
-   GOOGLE_CLIENT_ID=
-   GOOGLE_CLIENT_SECRET=

Run `npm start` or `npm run dev` to start the server

Access the application at `http://localhost:3000`